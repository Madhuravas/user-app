import Address from "../Address/address";
import Posts from "../Postes/postes";
import Albums from "../userAlbums/albums";


import "./EachUserData.css"


const EachUserData = (props) => {
  const { userData, posts } = props;
  const { name, phone, email, address, id } = userData

  const filterdPostsData = (posts || []).filter(eachUserPosts => {
    return eachUserPosts.userId === id
  })

  return (
    <li className="eachUser-card">
      <div className="user-details">
        <h1 className="name-head">Name:<span className="name"> {name}</span></h1>
        <h2 className="address-head">Address:</h2>
        <Address address={address} />
        <p className="head">Phone No: <span className="text">{phone}</span></p>
        <span className="head">Email: <span className="text">{email}</span></span>
        <h3>Images:</h3>
        <div className="image-coll">
        <Albums id={id} />
        
        </div>
      </div>
      <div className="post-card">
        <h1 className="post-card-head">Postes:</h1>
        {(filterdPostsData || []).map((eachpost) => {
          return <Posts post={eachpost} key={eachpost.id} />
        })}
      </div>
    </li>
  )
}

export default EachUserData




