import { Component } from "react"
import Photos from "../photos/photos.js"

import "./albums.js"

class Albums extends Component{
    state = {albums:""}

    componentDidMount(){
        const userId = this.props.id
        fetch(`https://jsonplaceholder.typicode.com/albums?userId=${userId}`)
        .then(response =>{
            return response.json()
        }).then(data =>{
             this.setState({albums:data})
        })
    }

    render(){
        const {albums} = this.state
        if(albums !== ""){
            return(
                <Photos albums={albums}/>
            )
        }
    
    }
}


export default Albums

