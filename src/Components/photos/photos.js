import { Component } from "react";

import "./photos.css"

class Photos extends Component {
    state = { photos: "" }

    componentDidMount() {
        const albumsId = this.props.albums[0].id

        fetch(`https://jsonplaceholder.typicode.com/photos?albumId=${albumsId}`)
            .then(res => {
                return res.json()
            }).then(data => {
                this.setState({ photos: data })
            })
    }

    render() {
        const { photos } = this.state

        if (photos !== "") {
            return (
                <>
                    <img className="userImage" src={photos[0].url} alt="user1" />
                    <img className="userImage" src={photos[1].url} alt="user1" />
                    <img className="userImage" src={photos[2].url} alt="user1" />
                    <img className="userImage" src={photos[3].url} alt="user1" />
                    <img className="userImage" src={photos[4].url} alt="user1" />
                </>
                
            )
        }

    }
}


export default Photos