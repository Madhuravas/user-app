import { Component } from "react";

import EachUserData from "../EachUserData/EachUserData";

import "./DisplayUser.css"

class DisplayUserData extends Component {
    state = {users:"",posts:""}

    componentDidMount() {
        fetch(" https://jsonplaceholder.typicode.com/users")
            .then(async res =>
                await res.json())
            .then((json) => {
                this.setState({
                    users: json
                });
            })

        fetch("https://jsonplaceholder.typicode.com/posts")
            .then(async res =>
                await res.json())
            .then((json) => {
                this.setState({
                    posts: json
                });
            })
    }

    render() {
        const { users,posts } = this.state
        return (
            <ul className="userList">
                {(users || []).map(eachuser => {
                    return <EachUserData key={eachuser.id} userData={eachuser} posts={posts}/>
                })
                }
            </ul>
        )
    }
}


export default DisplayUserData



