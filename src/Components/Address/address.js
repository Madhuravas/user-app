import "./address.css"

const Address = (props) =>{
    const { address } = props
    const {city,street,zipcode} = address

    return(
        <ul className="address">
        <li className="address-list">city: <span className="address-text">{city}</span></li>
        <li className="address-list">street: <span className="address-text">{street}</span></li>
        <li className="address-list">Zipcode: <span className="address-text">{zipcode}</span></li>
      </ul>
    )
}


export default Address