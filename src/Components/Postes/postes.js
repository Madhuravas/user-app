import "./postes.css"

const Posts = (props) =>{
   const {post} = props
   const {title,body} = post
    return (
        <div className="each-post-card">
          <h1 className="title" >{title}</h1>
          <p className="body">{body}</p>
        </div>
    )
}



export default Posts



